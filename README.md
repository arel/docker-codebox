# docker-codebox

## What's this?

IDE in a container.  Using https://github.com/FriendCode/codebox.

## How to build the container

```bash
cd docker-codebox
docker build -t codebox .
```

## How to run codebox IDE

### Step 1. Create SSH credentials (optional)

Create an `.ssh` directory for your codebox instance. This can go anywhere
but let's put it in a convenient place for this example. The codebox
server will run as `root`, so we must set the permissions on the
directory to be owned by `root`.

```bash
mkdir $HOME/codebox-ssh
chmod 700 $HOME/codebox-ssh
ssh-keygen -f $HOME/codebox-ssh/id_rsa
sudo chown -R root:root $HOME/codebox-ssh
```

### Step 2. Run Codebox

Assuming the ssh credentials are in the directory above, you
can pass it to codebox when you run it.

```bash
docker run -p 8000:8000 -v $HOME/codebox-ssh:/root/.ssh:ro codebox run project
```

You should now be able to 
If you skipped Step 1, you can run it without passing `.ssh` credentials
without the `-v` flag.

```bash
docker run -p 8000:8000 codebox run project
```

To run with basic **user:password** authentication use the `-u` flag:
```bash
docker run -p 8000:8000 -v $HOME/codebox-ssh:/root/.ssh:ro codebox -u you@example.com:abc123 run project
```

For all other options see the help screen:

```bash
docker run codebox --help
```